package com.nimvb.lib.base.service.io;

import lombok.Getter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;


public class ExtendedBufferReader extends BufferedReader {

    @Getter
    private long position = 0;
    private int readAheadLimit;

    public ExtendedBufferReader(Reader in, int sz) {
        super(in, sz);
    }

    public ExtendedBufferReader(Reader in) {
        super(in);

    }

    @Override
    public int read() throws IOException {
        int current = super.read();
        this.position += 1;
        return current;
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        int count = super.read(cbuf, off, len);
        this.position += count;
        return count;
    }

    @Override
    public void mark(int readAheadLimit) throws IOException {
        synchronized (this.lock){
            this.readAheadLimit = readAheadLimit;
        }
        super.mark(readAheadLimit);
    }

    @Override
    public void reset() throws IOException {
        synchronized (this.lock){
            this.position -= this.readAheadLimit;
            this.readAheadLimit = 0;
        }
        super.reset();
    }
}
