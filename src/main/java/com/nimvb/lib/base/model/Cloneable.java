package com.nimvb.lib.base.model;

public interface Cloneable<typeT> extends java.lang.Cloneable {
    typeT clone();
}
